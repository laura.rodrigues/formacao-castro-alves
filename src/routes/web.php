<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomePageController@index');
Route::get('/doacao', 'InscriptionController@doacao');
Route::post('/publico/inscricao', 'InscriptionController@store');
Route::get('/publico/curso/{course_id}', 'InscriptionController@create');
Route::get('/portoalegre', 'InscriptionController@portoalegre');
Route::get('/clube-do-livro', 'InscriptionController@clubeDoLivro');
Route::get('/publico/estados', 'EstadoController@index');
Route::get('/publico/estados/{id}', 'EstadoController@show');

Route::get('/admin', function () {
    return redirect('administracao');
});

Route::get('/administracao', 'SessionController@create');
Route::post('/administracao', 'SessionController@store');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/administracao/curso/{id}/inscricoes', 'CourseController@show');
    Route::post('/administracao/curso/{id}/enviar-emails', 'InscriptionsEmailController@store');

    Route::get('/administracao/logout', 'SessionController@destroy');
    Route::get('/administracao/dashboard', function () {
        return redirect('administracao/curso');
    });
    Route::get('/administracao/curso', 'CourseController@index');
    Route::get('/administracao/curso/cadastrar', 'CourseController@create');
    Route::get('/administracao/curso/{id}/editar', 'CourseController@edit');
    Route::put('/administracao/curso/{id}', 'CourseController@update');
    Route::get('/administracao/curso/{id}/apagar', 'CourseController@destroy');
    Route::post('/administracao/curso', 'CourseController@store');

    Route::get('/administracao/inscricoes', 'InscriptionController@index');
    Route::post('/administracao/inscricoes', 'InscriptionController@index');
    Route::post('/administracao/inscricoes/delete/{id}', 'InscriptionController@destroy');
    Route::get('/administracao/inscricoes/exportar', 'InscriptionReportController@index');
    Route::get('/administracao/inscricoes/exportarCourses/{id}', 'InscriptionReportController@reportFilter');
});

Route::get('/administracao/backup/importar/{file_name}', 'InscriptionImportController@store');

/*Route::get('/update', function () {
    foreach (\App\Models\Inscription::with(['course'])->cursor() as $inscription) {
        $classId = \Illuminate\Support\Facades\DB::table('classes')->select('id')->where(['course_id' => $inscription->course_id, 'status' => true, 'principal' => true])->first()->id;

        \Illuminate\Support\Facades\DB::table('inscriptions')->where(['id' => $inscription->id])->update(['class_id' => $classId]);
    }
});*/
