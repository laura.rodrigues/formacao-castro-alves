<?php

namespace App\Models;

use App\Models\CourseClass;
use App\Models\Inscription;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'titulo',
        'introducao',
        'ementa',
        'status',
        'principal',
        'seminario'
    ];

    protected $casts = [
        'status' => 'bool',
        'principal' => 'bool',
        'created_at'  => 'date:d/m/Y H:i:s',
    ];

    public $validate = [
        'titulo' => 'required',
    ];

    public $appends = [
        'created_at_display',
        'status_display',
        'principal_display',
    ];

    public function getActiveAndMainClass()
    {
        return $this->classes()->where(['status' => true])->first();
    }

    public function isHaveInscriptions()
    {
        return $this->inscriptions->count() > 0;
    }

    public function isNotHaveInscriptions()
    {
        return $this->inscriptions->count() === 0;
    }

    public function inscriptions()
    {
        return $this->hasMany(Inscription::class);
    }

    public function isCoursePrincipalNotExists()
    {
        return $this->where(['principal' => true])->count() === 0;
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('d/m/Y H:i:s', strtotime($this->created_at));
    }

    public function getStatusDisplayAttribute()
    {
        return $this->status ? 'Sim' : 'Não';
    }
    
    public function getIsSeminario()
    {
        return $this->seminario ? 'Sim' : 'Não';
    }

    public function getPrincipalDisplayAttribute()
    {
        return $this->principal ? 'Sim' : 'Não';
    }

    public function classes()
    {
        return $this->hasMany(CourseClass::class);
    }
}
