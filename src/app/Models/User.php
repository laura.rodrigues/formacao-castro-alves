<?php

namespace App\Models;

use App\User as BaseUser;

class User extends BaseUser
{
    public $validate = [
        'email' => 'required|email',
        'senha' => 'required',
    ];
}
