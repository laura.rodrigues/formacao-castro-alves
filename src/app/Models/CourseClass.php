<?php

namespace App\Models;

use App\Models\Course;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CourseClass extends Model
{
    protected $table = 'classes';

    const VALIDATE_FIELDS = [
        'titulo' => 'required',
        'course_id' => 'required|exists:courses,id'
    ];

    protected $fillable = [
        'titulo',
        'descricao',
        'status',
        'principal',
        'course_id'
    ];

    public $appends = [
        'status_display',
        'principal_display',
        'created_at_display',
        'count_inscriptions',
    ];

    public function associateCourseByRequest()
    {
        $data = array_merge(request()->post('turma'), [
            'course_id' => request()->route('id')
        ]);

        if (!$data['titulo']) {
            return null;
        }

        if (!isset($data['status'])) {
            $data['status'] = false;
        }

        if ($this->isCourseClassPrincipalNotExists()) {
            $data['principal'] = true;
        }

        if (isset($data['principal'])) {
            (new self)->where(['course_id' => $data['course_id']])->update(['principal' => false]);
        }

        if (isset($data['id'])) {
            return (new self)->where(['id' => $data['id'], 'course_id' => $data['course_id']])->update($data);
        }

        return $this->firstOrCreate($data);
    }

    public function getCountInscriptionsAttribute()
    {
        return DB::table('inscriptions')->where(['class_id' => $this->id])->count();
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('d/m/Y H:i:s', strtotime($this->created_at));
    }

    public function getStatusDisplayAttribute()
    {
        return $this->status ? 'Sim' : 'Não';
    }

    public function getPrincipalDisplayAttribute()
    {
        return $this->principal ? 'Sim' : 'Não';
    }

    public function isCourseClassPrincipalNotExists()
    {
        return $this->where(['principal' => true])->count() === 0;
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
