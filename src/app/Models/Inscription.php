<?php

namespace App\Models;

use App\Models\Course;
use App\Models\CourseClass;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    const VALIDATE_FIELDS = [
        'nome' => 'required',
        'idade' => 'required',
        'email' => 'required',
        'whatsapp' => 'required',
        'estado' => 'required',
        'cidade' => 'required',
        'estado' => 'required',
        'curso_id' => 'required|exists:courses,id'
    ];

    protected $fillable = [
        'nome',
        'idade',
        'email',
        'whatsapp',
        'estado',
        'cidade',
        'estado',
        'estudante',
        'estudante_info',
        'trabalha',
        'trabalha_info',
        'movimento',
        'movimento_info',
        'bairro',
        'course_id',
        'class_id'
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y H:i:s',
    ];

    public $appends = [
        'created_at_display',
    ];

    public function getCreatedAtDisplayAttribute()
    {
        return date('d/m/Y H:i:s', strtotime($this->created_at));
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function class()
    {
        return $this->belongsTo(CourseClass::class);
    }
}
