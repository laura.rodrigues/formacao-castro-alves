<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'id',
        'id_uf',
        'nome',
    ];
}
