<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'states';

    protected $fillable = [
        'id',
        'sigla',
        'nome',
    ];

    public function cidades()
    {
        return $this->hasMany(Cidade::class, 'state_id', 'id');
    }
}
