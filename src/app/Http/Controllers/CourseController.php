<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Course;
use App\Models\CourseClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    private $course = null;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    public function index(Request $request)
    {
        $courses = Course::with(['classes'])->orderBy('created_at', 'DESC')->get();

        return view('admin.course.index', compact('courses'));
    }

    public function create(Request $request)
    {
        return view('admin.course.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->course->validate);

        try {
            $data = $request->only(['titulo', 'introducao', 'ementa', 'status', 'principal', 'seminario']);
            

            $course = $this->course->firstOrCreate($data);

            return redirect()->to('administracao/curso');
        } catch (Exception $e) {
            return redirect()->back();
        }
    }

    public function show(Request $request, $id)
    {
        $course = Course::with(['classes'])->findOrFail($id);

        $course->inscriptions = DB::table('inscriptions')->where(['course_id' => $id])->get();

        return response()->json(['course' => $course], 200);
    }

    public function edit(Request $request, $id)
    {
        $course = Course::with(['classes'])->findOrFail($id);

        return view('admin.course.edit', compact('course'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->course->validate);
        
        $request['seminario'] = (!isset($request['seminario'])) ? 0 : 1;
        
        try {
            $data = $request->only(['titulo', 'introducao', 'ementa', 'status', 'principal', 'seminario']);

            /*if ($this->course->isCoursePrincipalNotExists()) {
                $data['principal'] = true;
            }

            $this->course->where(['status' => true])->update(['principal' => false]);*/

            $this->course->findOrFail($id)->update($data);

            try {
                (new CourseClass)->associateCourseByRequest();
            } catch (Exception $e) {
            }

            return redirect()->back();
        } catch (Exception $e) {
            return redirect()->back();
        }
    }

    public function destroy(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->delete();

        return redirect('administracao/curso');
    }
}
