<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Rap2hpoutre\FastExcel\FastExcel;

class InscriptionImportController extends Controller
{
    public function store(Request $request, $file_name)
    {
        $file_path = storage_path("backup_inscriptions/{$file_name}");

        (new FastExcel)->import($file_path, function ($line) {
            try {
                $inscription = DB::table('inscriptions_backup')->where([
                    'email' => strtolower($line['EMAIL']),
                    'course_id' => $line['CURSO'],
                ])->first();

                if (is_null($inscription)) {
                    $date_without_hours = implode('-', array_reverse(explode('/', substr($line['INSCRITO EM'], 0, 10)))) . ' 00:00:00';

                    $data = [
                        'nome' => $line['NOME COMPLETO'],
                        'idade' => $line['IDADE'],
                        'email' => $line['EMAIL'],
                        'whatsapp' => $line['WHATSAPP'],
                        'estado' => $line['ESTADO'],
                        'cidade' => $line['CIDADE'],
                        'estudante' => strtolower($line['ESTUDANTE ?']) == 'não' ? 0 : 1,
                        'estudante_info' => $line['SE SIM, ESTUDA ONDE? QUAL CURSO E/OU ANO?'],
                        'trabalha' => strtolower($line['TRABALHA ?']) == 'não' ? 0 : 1,
                        'trabalha_info' => $line['SE SIM, TRABALHA ONDE?'],
                        'movimento_info' => $line['SE SIM, QUAL?'],
                        'bairro' => $line['BAIRRO'],
                        'course_id' => $line['CURSO'],
                        'class_id' => $line['TURMA'],
                        'created_at' => $date_without_hours
                    ];

                    if (isset($line['PARTICIPA MOVIMENTO ?'])) {
                        $data['movimento'] = strtolower($line['PARTICIPA MOVIMENTO ?']) == 'não' ? 0 : 1;
                    }

                    if (isset($line['PARTICIPA DE ALGUM COLETIVO OU MOVIMENTO ?'])) {
                        $data['movimento_info'] = strtolower($line['PARTICIPA DE ALGUM COLETIVO OU MOVIMENTO ?']) == 'não' ? 0 : 1;
                    }

                    DB::table('inscriptions_backup')->insert($data);
                }
            } catch (Exception $e) {
                Log::debug($e->getMessage());
            }
        })->toArray();

        return response()->json(['success']);
    }
}
