<?php

namespace App\Http\Controllers;

use App\Models\Inscricao;
use App\Models\Configuracao;
use App\Models\Destinatario;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $destinatario = null;

        $current_tab = $request->get('tab') ? $request->get('tab') : 1;

        $cursosTab = collect([]);

        return view('admin.dashboard.index', compact('current_tab', 'cursosTab'));
    }
}
