<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Course;
use App\Models\CourseClass;
use App\Models\Estado as State;
use App\Models\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class InscriptionController extends Controller
{
    const MESSAGE_SUCCESS = 'Obrigado por se inscrever em nossos cursos. Você receberá no seu e-mail as instruções para acessar as aulas. Não esqueça de verificar a caixa spam, caso o e-mail demore a chegar.';

    const MESSAGE_FAILURE = 'Não foi possível realizar usa inscrição. Tente novamente em instantes.';

    public function index(Request $request)
    {
        $search = $request->get('search');
        
        if(!empty($search)) {
            $inscriptions = Inscription::where('email', 'LIKE', '%'.$search.'%')
                            ->orWhere('nome', 'LIKE', '%'.$search.'%')
                            ->paginate(30);
            
        } else {
            $inscriptions = Inscription::orderBy('id', 'DESC')->with(['course', 'class'])->paginate(30);    
        }
        
        $courses = Course::all();
        
        return view('admin.inscription.index', compact('inscriptions', 'search', 'courses'));
    }

    public function create(Request $request, $courseId)
    {
        try {
            $isShowIntroduction = false;

            $states = State::all();

            $course = Course::findOrFail($courseId);

            $courses = Course::orderBy('created_at', 'DESC')->get();

            $classes = CourseClass::where(['course_id' => $course->id, 'status' => true])->get();

            return view('public.index', compact('course', 'courses', 'isShowIntroduction', 'states', 'classes'));
        } catch (Exception $e) {
            return redirect()->back();
        }
    }
    
    public function portoalegre()
    {
        try {
            $isShowIntroduction = false;

            $states = State::all();

            $course = Course::findOrFail(6);

            $courses = Course::orderBy('created_at', 'DESC')->get();

            $classes = CourseClass::where(['course_id' => $course->id, 'status' => true])->get();

            return view('public.index', compact('course', 'courses', 'isShowIntroduction', 'states', 'classes'));
        } catch (Exception $e) {
            return redirect()->back();
        }
    }
    
    
    public function clubeDoLivro()
    {
        try {
            $isShowIntroduction = false;

            $states = State::all();

            $course = Course::findOrFail(7);

            $courses = Course::orderBy('created_at', 'DESC')->get();

            $classes = CourseClass::where(['course_id' => $course->id, 'status' => true])->get();

            return view('public.index', compact('course', 'courses', 'isShowIntroduction', 'states', 'classes'));
        } catch (Exception $e) {
            return redirect()->back();
        }
    }
    

    public function doacao() {

        $courses = Course::orderBy('created_at', 'DESC')->get();

        return view('public.sections.section-five', compact('courses'));
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, Inscription::VALIDATE_FIELDS);

            $courseId = (int) $request->post('curso_id');
            $classId  = (int) $request->post('class_id');
            
            if(empty($classId)) {
                $classIdDefault = CourseClass::where(['course_id' => $courseId, 'status' => true])->first();                
            }

            $data = $request->only([
                'nome',
                'idade',
                'email',
                'whatsapp',
                'estado',
                'cidade',
                'estado',
                'estudante',
                'estudante_info',
                'trabalha',
                'trabalha_info',
                'movimento',
                'movimento_info',
                'bairro'
            ]);

            //$class_main_id = null;

            /*try {
                $classMain = CourseClass::where(['course_id' => $courseId, 'status' => true, 'principal' => true])->first();

                if ($classMain) {
                    $class_main_id = $classMain->id;
                }
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }*/

            $data = array_merge($data, ['course_id' => $courseId, 'class_id' => $classId]);

            $inscription = Inscription::firstOrCreate($data);

            try {
                $viewEmail = ($inscription->course->seminario == 0) ? 'emails.inscription-success' : 'emails.inscription-seminario';
                
                Mail::send($viewEmail, ['inscription' => $inscription], function ($message) use ($inscription) {
                    $message->subject('Seja bem-vindo!');

                    $message->from(env('MAIL_FROM_ADDRESS'), 'Formação Castro Alves');

                    $message->to($inscription->email, $inscription->nome);
                });
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'message' => self::MESSAGE_SUCCESS]);
            }

            return redirect()->back()->with(['success' => true, 'message' => self::MESSAGE_SUCCESS]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            if ($request->ajax()) {
                return response()->json(['success' => false, 'error' => true,  'message' => self::MESSAGE_FAILURE]);
            }

            return redirect()->back(['success' => false, 'error' => true,  'message' => self::MESSAGE_FAILURE], 400);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
    
    
    public function destroy($id)
    {
        
        $inscricao = Inscription::find($id);
        $inscricao->delete();
        
        return redirect('/administracao/inscricoes')->with('sucess', 'Inscrição removida com sucesso.');
        
    }
    
}
