<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseClass;
use Illuminate\Http\Request;
use App\Models\Estado as State;


class HomePageController extends Controller
{
    public function index(Request $request)
    {
        $isShowIntroduction = true;

        $states = State::all();

        $course = Course::where(['status' => true, 'principal' => true])->first();

        //$courses = Course::where(['seminario' => 0])->orderBy('created_at', 'DESC')->get();
        $courses = Course::orderBy('created_at', 'DESC')->get();

        if (is_null($course)) {
            $course = new Course;
        }

        $classes = CourseClass::where(['course_id' => $course->id, 'status' => true])->get();

        return view('public.index', compact('course', 'isShowIntroduction', 'courses', 'states', 'classes'));
    }
}
