<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class InscriptionsEmailController extends Controller
{
    const MESSAGE_SUCCESS = 'O email personalizado foi enviados para os inscritos com sucesso.';

    const MESSAGE_FAILURE = 'Não foi possível enviar o email personalizado para os inscritos. Tente novamente em instantes.';


    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'turma_id' => 'required',
            'content_email' => 'required'
        ]);

        try {
            if (!env('MAIL_SEND_MOCKED')) {
                $class_id = $request->post('turma_id');
                $email_text = $request->post('content_email');

                $course = DB::table('courses')->where(['id' => $id])->first();

                $inscriptions = DB::table('inscriptions')->where(['course_id' => $id, 'class_id' => $class_id])->cursor();

                foreach ($inscriptions as $inscription) {
                    try {
                        Mail::send('emails.inscription-customized', ['texto_html' => $email_text], function ($message) use ($inscription, $course) {
                            $message->subject("Aviso! {$course->titulo}");

                            $message->from(env('MAIL_FROM_ADDRESS'), 'Formação Castro Alves');

                            $message->to($inscription->email, $inscription->nome);
                        });
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                }
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'message' => self::MESSAGE_SUCCESS]);
            }

            return redirect()->back()->with(['success' => true, 'message' => self::MESSAGE_SUCCESS]);
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['success' => false, 'error' => true,  'message' => self::MESSAGE_FAILURE]);
            }

            return redirect()->back(['success' => false, 'error' => true,  'message' => self::MESSAGE_FAILURE], 400);
        }
    }
}
