<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Estado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstadoController extends Controller
{
    public function index(Request $request)
    {
        try {
            $estados = Estado::all();

            return $this->response([
                'estados' => $estados,
            ], 'Lista de estados consultados com sucesso.', 200);
        } catch (Exception $e) {
            return $this->response([], 'Não foi possível consultar a lista de estados.', 400);
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $estado = Estado::with('cidades')->find($id);

            return $this->response([
                'estado' => $estado,
            ], 'O estado consultados com sucesso.', 200);
        } catch (Exception $e) {
            return $this->response([], 'Não foi possível consultar o estado.', 400);
        }
    }
}
