<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Inscription;
use Rap2hpoutre\FastExcel\FastExcel;

class InscriptionReportController extends Controller
{
    public function index()
    {
        try {
            return (new FastExcel($this->inscriptionsGenerator()))->withoutHeaders()->download('inscritos_' . date('dmY_His') . '.xlsx');
        } catch (Exception $e) {
            return redirect()->back();
        }
    }
    
    public function reportFilter($idCourse) {
        try {
            return (new FastExcel($this->inscriptionsGeneratorFilter($idCourse)))->withoutHeaders()->download('inscritos_' . date('dmY_His') . '.xlsx');
        } catch (Exception $e) {
            return redirect()->back();
        }
    }
    
    
    public function inscriptionsGeneratorFilter($idCourse)
    {
        yield [
            'ID',
            'ESTADO',
            'CIDADE',
            'NOME COMPLETO',
            'IDADE',
            'EMAIL',
            'WHATSAPP',
            'BAIRRO',
            'CURSO',
            'TURMA',
            'ESTUDANTE ?',
            strtoupper('Se sim, estuda onde? Qual curso e/ou ano?'),
            'TRABALHA ?',
            strtoupper('Se sim, trabalha onde?'),
            strtoupper('Participa de algum coletivo ou movimento ?'),
            strtoupper('Se sim, qual?'),
            'INSCRITO EM'
        ];

        foreach (Inscription::where(['course_id' => $idCourse])->with(['course', 'class'])->cursor() as $inscription) {
            yield [
                $inscription->id,
                $inscription->estado,
                $inscription->cidade,
                $inscription->nome,
                $inscription->idade,
                $inscription->email,
                $inscription->whatsapp,
                $inscription->bairro,
                $inscription->course->titulo,
                $inscription->class->titulo,
                $inscription->estudante ? 'Sim' : 'Não',
                $inscription->estudante_info,
                $inscription->trabalha ? 'Sim' : 'Não',
                $inscription->trabalha_info,
                $inscription->movimento ? 'Sim' : 'Não',
                $inscription->movimento_info,
                $inscription->created_at_display,
            ];
        }
    }
    

    private function inscriptionsGenerator()
    {
        yield [
            'ID',
            'ESTADO',
            'CIDADE',
            'NOME COMPLETO',
            'IDADE',
            'EMAIL',
            'WHATSAPP',
            'BAIRRO',
            'CURSO',
            'TURMA',
            'ESTUDANTE ?',
            strtoupper('Se sim, estuda onde? Qual curso e/ou ano?'),
            'TRABALHA ?',
            strtoupper('Se sim, trabalha onde?'),
            strtoupper('Participa de algum coletivo ou movimento ?'),
            strtoupper('Se sim, qual?'),
            'INSCRITO EM'
        ];

        foreach (Inscription::with(['course', 'class'])->cursor() as $inscription) {
            yield [
                $inscription->id,
                $inscription->estado,
                $inscription->cidade,
                $inscription->nome,
                $inscription->idade,
                $inscription->email,
                $inscription->whatsapp,
                $inscription->bairro,
                $inscription->course->titulo,
                $inscription->class->titulo,
                $inscription->estudante ? 'Sim' : 'Não',
                $inscription->estudante_info,
                $inscription->trabalha ? 'Sim' : 'Não',
                $inscription->trabalha_info,
                $inscription->movimento ? 'Sim' : 'Não',
                $inscription->movimento_info,
                $inscription->created_at_display,
            ];
        }
    }
}
