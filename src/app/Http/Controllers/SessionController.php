<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    private $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(Request $request)
    {
        return view('admin.sessao.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->user->validate);

        try {
            $dadosFormulario = $request->only(['email', 'password']);

            $user = $this->user->where($dadosFormulario)->firstOrFail();

            Auth::login($user);

            return redirect('administracao/dashboard');
        } catch (Exception $e) {
            return redirect('/administracao');
        }
    }

    public function destroy(Request $request)
    {
        Auth::logout();

        return redirect('/administracao');
    }
}
