<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create('inscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('idade');
            $table->string('email');
            $table->string('whatsapp');
            $table->string('estado');
            $table->string('cidade');
            $table->boolean('estudante')->default(false);
            $table->string('estudante_info')->nullable();
            $table->boolean('trabalha')->default(false);
            $table->string('trabalha_info')->nullable();
            $table->boolean('movimento')->default(false);
            $table->string('movimento_info')->nullable();

            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('inscriptions');
    }
}
