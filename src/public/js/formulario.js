$(document).ready(function () {
    $(".number_whatsapp").mask("(00) 00000-0000");
});

$("#inscriptionForm").on("submit", function (e) {
    e.preventDefault();

    const $form = $(this);
    const url = $form.attr("action");
    const type = $form.attr("method");
    const data = $form.serialize();
    const $button = $form.find(".btn-primary");

    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: "json",
        cache: false,
        beforeSend: function () {
            $(".overlay").removeClass("hidden");

            $(".feedback").html(``);

            $(".overlay span")
                .addClass("text-center")
                .html("Registrado sua inscrição... <br /> aguarde.");

            $button.attr("disabled", true).html("Enviado ...");

            $form.find("input, select, textarea").prop("disabled", true);
        },
        success: function (response) {
            $(".feedback").html(`
                <div class="alert alert-success mt-3">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    ${response.message}
                </div>
            `);

            $form[0].reset();
        },
        error: function (response) {
            if (parseInt(response.status) == 422) {
                $(".feedback").html(`
                    <div class="alert alert-danger mt-3">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Não foi possível realizar sua inscrição. Preencha todos os campos obrigatórios e tente novamente.
                    </div>
                `);
            } else {
                $(".feedback").html(`
                    <div class="alert alert-danger mt-3">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Não foi possível realizar sua inscrição. Tente novamente.
                    </div>
                `);
            }
        },
        complete: function () {
            $button.attr("disabled", false).html("Inscrever-se");
            $form.find("input, select, textarea").prop("disabled", false);
            $(".overlay").addClass("hidden");
        },
    });

    return true;
});

$(".estado_id").on("change", function (e) {
    const estado_id = $(this).find(":selected").data("estadoId");

    $.ajax({
        type: "GET",
        url: `${window.location.origin}/publico/estados/${estado_id}`,
        beforeSend: function () {
            $(".cidade_id").html(
                `<option value="" selected>Carregando ...</option>`
            );
        },
        success: function (response) {
            const cidadesJSON = response.docs.estado.cidades;
            let templateOptions = `<option value="" selected>...</option>`;

            cidadesJSON.forEach((cidade) => {
                templateOptions += `<option value="${cidade.name}">${cidade.name}</option>`;
            });

            $(".cidade_id").html(templateOptions);
        },
        error: function (response) {},
        complete: function () {},
    });
});
