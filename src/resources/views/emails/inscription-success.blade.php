<p>
    <img style='height: 75%; width: 75%; object-fit: contain' src="{!! url('images/banner.jpeg') !!}"/>
</p>

<br />
<div style='height: 75%; width: 75%; object-fit: contain'>
    <p><b>Parabéns pela inscrição e seja bem-vindo (a) ao curso online {{ $inscription->course->titulo }}. Para ter acesso às aulas siga as instruções abaixo:</b></p>
    
    1 - As aulas serão ministradas em uma plataforma de videoconferência, onde somente o professor e o mediador da sala terão o microfone aberto.<br />
    2 - Não é necessário ter nenhum aplicativo para acessar a plataforma, somentente um navegador (chrome ou firefox) atualizado em versões recentes.<br />
    3 - Todos os dias de aula até às 10h, você receberá um link de acesso a sala da aula.<br />
    4 - Caso não receber o link até esse horário, envie envie um e-mail avisando.<br />
    5 - Teremos aulas na segunda, quarta e sexta nas três semanas de curso, todas de 14 às 17h para a turma A e aulas terça, quinta as 19 horas e sábado as 14 horas para turma B.<br />
    6 - As aulas terão duração de 3 horas, sendo*:<br />
    <ul>
        <li>1h40 de exposição inicial</li>
        <li>20 min. de intervalo (tempo para enviar as dúvidas)</li>
        <li>1h de esclarecimento das dúvidas e colocações</li>
    </ul>
    7 - Para quem chegou agora acesse as aulas ministradas até agora: <br />
    <ul>
        <li>Aula inaugural - A Importância do estudo para ação politica, por Manuela D’Ávila  https://youtu.be/UmtDrNwDAAU</li>
        <li>2ª Aula - A formação do mundo contemporâneo, por Júlio Vellozo https://youtu.be/M9scVa6hLJs </li>
        <li>3ª Aula - Interpretações do Brasil Moderno, por André Tokarski https://youtu.be/Yn9560I6yAs</li>
    </ul>
    
    <p>
        <b>Siga nossas redes para maiores informações:</b><br />
        Instagram: @UJS_Brasil<br />
        Twitter: @UJSBRASIL<br />
        Facebook: UJS - União da Juventude Socialista<br />
        Podcast: /Molotov Podcast (Spotify, Deezer, Itunes e SoundCloud)<br />
        <a href="http://ujs.org.br">ujs.org.br</a><br />
        <a href="http://esefossevc.com.br">esefossevc.com.br</a><br />
        <a href="http://grabois.org.br">grabois.org.br</a><br />
    </p>
    
    <p>
        <b>
            Qualquer dúvida estamos a disposição<br />
            Bons Estudos 
        </b>
    </p>
</div>