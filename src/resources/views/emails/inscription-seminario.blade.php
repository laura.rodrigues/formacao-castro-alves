<p>
    <img style='height: 75%; width: 75%; object-fit: contain' src="{!! url('images/banner.jpeg') !!}"/>
</p>

<div style='height: 75%; width: 75%; object-fit: contain'>
    <p><b>Parabéns pela inscrição e seja bem-vindo (a) ao seminário online "{{ $inscription->course->titulo }}".</b></p>
    
    <p>Confira a programação:</p>

    {!!$inscription->course->ementa!!}
    
    <p>
        <b>
            Qualquer dúvida estamos a disposição.<br />
            Bons Estudos!
        </b>
    </p>

</div>