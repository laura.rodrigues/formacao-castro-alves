<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ url('/images/favicon.png') }}">

    <title>Formação Castro Alves</title>
    <meta name='application-name' content='Formação Castro Alves'>
    <meta name='author' content='UJS'>
    <meta name='description' content='Formação Castro Alves'>
    <meta name='language' content='pt-br'>
    <meta name='twitter:site' content='Formação Castro Alves'>
    <meta name='twitter:description' content='Formação Castro Alves'>
    <meta name='twitter:domain' content='http://formacaocastroalves.com.br'>
    <meta name='apple-mobile-web-app-title' content='Formação Castro Alves'>
    <meta property='og:locale' content='pt_BR'>
    <meta property='og:title' content='Formação Castro Alves'>
    <meta property='og:description' content='Formação Castro Alves'>
    <meta property='og:url' content='http://formacaocastroalves.com.br'>
    <meta itemprop='url' content='http://formacaocastroalves.com.br'>
    <meta itemprop='image' content=''>
    <meta itemprop='description' content='Formação Castro Alves'>

    <link rel="stylesheet" href="{{ url('/libs/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/libs/font-awesome/css/font-awesome.min.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163920784-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-163920784-1');
    </script>

    <style>
        :root {
            --primary: #2e959f;
            --secondary: #ea5b25;
            --padding: 0 50px;
        }

        html,
        body {
            background-color: #f5f5f5;
            color: #636b6f;
            font-family: sans-serif;
            height: 100%;
            margin: 0;
            box-sizing: border-box;
            overflow: auto;
            padding: var(--padding);
        }

        @media (max-width: 720px) {

            html,
            body {
                padding: 0;
            }
        }
    </style>
</head>

<body>
    @include('public.navbar')

    @yield('content')

    <script src="{{ url('/libs/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ url('/libs/jquery.mask.min.js') }}"></script>
    <script src="{{ url('/libs/bootstrap.min.js') }}"></script>
    <script src="{{ url('/js/formulario.js') }}"></script>
</body>

</html>
