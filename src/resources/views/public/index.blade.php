@extends('public.layout')

@section('content')

<div class="overlay hidden">
    <i class="fa fa-circle-o-notch fa-spin fa-4x"></i>
    <span class="text-center">Registrado sua inscrição... <br /> aguarde.</span>
</div>

@include('public.sections.section-one')

@include('public.sections.section-two')

@include('public.sections.section-three')

@include('public.sections.section-four')

@include('public.sections.footer')

@endsection

<style>
    .overlay {
        background-color: rgba(255, 255, 255, 0.75);
        height: 100vh;
        width: 100vw;
        position: fixed;
        z-index: 2;
        top: 0;

        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }

    .overlay .fa {
        color: #000;
    }

    .overlay span {
        font-weight: bold;
        color: #000;
        font-size: 18px;
    }

    .card {
        border: 0 !important;
        background-color: #f5f5f5 !important;
    }

    .card-header {
        color: #45AFB9 !important;
        border: 0 !important;
        font-size: 20px;
        padding-left: 0 !important;
    }

    .hidden {
        display: none;
    }
</style>
