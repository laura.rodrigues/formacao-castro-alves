<nav class="navbar navbar-dark navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('/') }}">Início <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-target="#navToggleCommercial" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Cursos
                </a>
                <div class="dropdown-menu" id="navToggleCommercial">
                    @foreach ($courses as $course)
                    <a class="dropdown-item" href="{{ url('/publico/curso/' . $course->id) }}">{{ $course->titulo }}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#inscricao">Inscreva-se</a>
            </li>
        </ul>
    </div>
</nav>

<style>
    .navbar {
        border: none !important;
        background-color: var(--primary);
    }

    .navbar-nav li {
        margin-right: 10px;
    }

    .navbar-nav li>a {
        color: #FFF;
        font-weight: bold;
        font-size: 20px;
    }
</style>
