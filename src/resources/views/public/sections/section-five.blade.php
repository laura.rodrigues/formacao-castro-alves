@extends('public.layout')

@section('content')


@include('public.sections.section-one')

    <iframe src="https://ocupa.org.br/campanha/arrecadacao/inscricao" width="100%" height="1200" frameborder="0"></iframe>

@include('public.sections.footer')

@endsection


<style>
    .overlay {
        background-color: rgba(255, 255, 255, 0.75);
        height: 100vh;
        width: 100vw;
        position: fixed;
        z-index: 2;
        top: 0;

        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }

    .overlay .fa {
        color: #000;
    }

    .overlay span {
        font-weight: bold;
        color: #000;
        font-size: 18px;
    }

    .card {
        border: 0 !important;
        background-color: #f5f5f5 !important;
    }

    .card-header {
        color: #45AFB9 !important;
        border: 0 !important;
        font-size: 20px;
        padding-left: 0 !important;
    }

    .hidden {
        display: none;
    }
</style>
