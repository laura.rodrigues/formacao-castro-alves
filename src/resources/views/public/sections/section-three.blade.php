<section class="sectionTwo">
    <div class="card cardCourse">
        <div class="card-header d-flex justify-content-center flex-column">
            @if (!is_null($isShowIntroduction) && $isShowIntroduction === true)
            <div>
                <h3>Curso de destaque</h3>
            </div>
            @endif
            <div>
                <h5>{{ $course->titulo }}</h5>
            </div>
        </div>
        @if(!empty($course->introducao))
        <div class="card-body">
            <div class="title">
                Introdução
            </div>
            <div>
                {!! $course->introducao !!}
            </div>
        </div>
        @endif
    </div>

    <div class="card cardCourse mt-3">
        <div class="card-body card">
            <div class="title mb-3" href="#multiCollapseExample1" data-toggle="collapse">
                @if($course->seminario == 0)
                    Aulas e Horários <small>(ver detalhes)</small>
                    <i class="fa fa-chevron-down pull-right"></i>
                @else
                    Programação
                @endif
            </div>
            <div class="collapse {{ $isShowIntroduction ? '' : 'show' }}" id="multiCollapseExample1">
                {!! $course->ementa !!}
            </div>
        </div>
    </div>
</section>

<style>
    .cardCourse {
        margin-top: 30px;
    }

    .cardCourse .card-header {
        background-color: #45AFB9 !important;
        color: #fff !important;
        text-align: center;
        flex-direction: column;
    }

    .cardCourse .card-header h3 {
        font-size: 2.0rem;
    }

    .cardCourse .card-header h5 {
        font-size: 1.3rem;
    }

    .cardCourse .card-body .title {
        color: #45AFB9;
        text-align: center;
        font-weight: bold;
        font-size: 20px;
        cursor: pointer;
    }

    .cardCourse .card-body {
        text-align: justify;
    }

    .cardCourse .card-body p {
        width: 100%;
    }

    @media(max-width: 750px) {
        .cardCourse {
            margin-top: 0px;
        }
    }
</style>
