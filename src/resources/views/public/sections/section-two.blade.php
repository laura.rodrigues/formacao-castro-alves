@if (!is_null($isShowIntroduction) && $isShowIntroduction === true)
<div class="introduction">
    <h3>Bem vindos a Escola de Formação Política Castro Alves</h3>

    <h5>Aqui você vai encontrar <strong>cursos de formação</strong> básica e avançada nas mais variadas <br /> vertentes dentro do <strong>Marxismo</strong> e <strong>teorias críticas sociais</strong>.</h5>

    <hr />
</div>


<style>
    @media (max-width: 700px) {
        .doacao {
            margin-bottom: 40px;
        }
        
        .doacao a {
            margin-bottom: 10px;
        }
    }
    @media (max-width: 700px) {
        .banner-seminarios {
            margin-bottom: 10px;
        }
    }
    
</style>

<div class="text-center doacao">
    <span class="banner-seminarios">
        <a href="{{ url('/clube-do-livro') }}" style="padding: 10px;">
            <img src="{{ url('images/clube-do-livro.jpeg') }}" title="clube do livro" alt="clube do livro" width="300" />
        </a>
    </span>
    
    <span class="banner-seminarios">
        <a href="{{ url('/portoalegre') }}">
            <img src="{{ url('images/seminario-covid-19.jpeg') }}" title="seminario covid-19" alt="seminario covid-19" width="300" />
        </a>
    </span>
</div>

@else
<div class="introductionEmpty">
    <hr />
</div>
@endif

<style>
    .introduction {
        text-align: center;
        min-height: 220px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    .introductionEmpty {
        margin-top: 40px;
        width: 70%;
        margin-left: 15%;
    }

    .introduction h3 {
        letter-spacing: 1.5px;
        font-size: 1.7rem;
    }

    .introduction h5 {
        font-weight: 100;
        margin-top: 10px;
        font-size: 1.2rem;
    }

    .introduction hr {
        width: 70%;
        margin-top: 30px;
    }

    @media(max-width: 700px) {
        .introduction {
            min-height: 300px;
            height: 70%;
        }
    }

    @media(max-width: 750px) {
        .introduction {
            height: 35%;
        }

        .introduction hr {
            display: none;
        }

        .introduction h3 {
            letter-spacing: 0px;
            margin-bottom: 0;
        }
    }
</style>
