<section class="sectionOne">
</section>

<style>
    .sectionOne {
        background-image: url('/images/banner.jpeg');
        height: 50%;
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
    }

    @media (max-width: 750px) {
        .sectionOne {
            height: 19.5%;
            background-size: contain;
            margin-top: -5px;
        }
    }
</style>
