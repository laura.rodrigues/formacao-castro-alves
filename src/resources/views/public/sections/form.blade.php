<form action="{{ url('publico/inscricao') }}" method="POST" id="inscriptionForm">
    @csrf

    <input type="hidden" name="curso_id" value="{{ $course->id }}">

    <div class="feedback text-center"></div>

    <div class="form-row">
        <div class="col-sm-6 col-12">
            <div class="form-group">
                <label for="nome">Nome Completo *</label>
                <input type="text" class="form-control" id="nome" name="nome" required>
            </div>
        </div>
        <div class="col-sm-3 col-3">
            <div class="form-group">
                <label for="idade">Idade *</label>
                <input type="number" class="form-control" id="idade" name="idade" required>
            </div>
        </div>
        <div class="col-sm-3 col-9">
            <div class="form-group">
                <label for="email">E-mail *</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-4 col-12">
            <div class="form-group">
                <label for="whatsapp">Whatsapp (DDD + número) *</label>
                {{--<input type="text" class="form-control number_whatsapp" id="whatsapp" name="whatsapp" required>--}}
                <input type="text" class="form-control" id="whatsapp" name="whatsapp" required>
            </div>
        </div>
        @if($course->seminario == 1)
        <div class="col-sm-3 col-12">
        @else
        <div class="col-sm-4 col-12">
        @endif
            <div class="form-group">
                <label for="estado">Estado *</label>
                <select id="estado" name="estado" class="form-control estado_id" required>
                    <option value="" selected>Escolher...</option>

                    @foreach($states as $item)
                    <option value="{{ $item->name }}" data-estado-id="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @if($course->seminario == 1)
        <div class="col-sm-3 col-12">
        @else
        <div class="col-sm-4 col-12">
        @endif
            <div class="form-group">
                <label for="cidade">Cidade *</label>
                <select id="cidade" name="cidade" class="form-control cidade_id" required>
                    <option value="" selected>Escolher...</option>
                </select>
            </div>
        </div>
        @if($course->seminario == 1)
            <div class="col-sm-2 col-12">
                <div class="form-group">
                    <label for="bairro">Bairro *</label>
                    <input type="text" class="form-control" id="bairro" name="bairro" required/>
                </div>
            </div>
        @endif
    </div>

    <div class="form-row">
        <div class="col-sm-2 col-12">
            <div class="form-group">
                <label>Estudante ? *</label> <br />
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="estudante" id="estudante_sim" value="1">
                    <label class="form-check-label" for="estudante_sim">Sim</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="estudante" id="estudante_nao" value="0">
                    <label class="form-check-label" for="estudante_nao">Não</label>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-12">
            <div class="form-group">
                <label for="estudante_info">Se sim, estuda onde? Qual curso e/ou ano?</label>
                <input type="text" class="form-control" id="estudante_info" name="estudante_info">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-2 col-12">
            <div class="form-group">
                <label>Trabalha ? *</label> <br />
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trabalha" id="trabalha_sim" value="1">
                    <label class="form-check-label" for="trabalha_sim">Sim</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trabalha" id="trabalha_nao" value="0">
                    <label class="form-check-label" for="trabalha_nao">Não</label>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-12">
            <div class="form-group">
                <label for="trabalha_info">Se sim, trabalha onde?</label>
                <input type="text" class="form-control" id="trabalha_info" name="trabalha_info">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-2 col-12">
            <div class="form-group">
                <label>Participa de algum coletivo ou movimento ? *</label> <br />
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="movimento" id="movimento_sim" value="1">
                    <label class="form-check-label" for="movimento_sim">Sim</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="movimento" id="movimento_nao" value="0">
                    <label class="form-check-label" for="movimento_nao">Não</label>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-12">
            <div class="form-group">
                <label for="movimento_info">Se sim, qual?</label>
                <input type="text" class="form-control" id="movimento_info" name="movimento_info">
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12">
            <div class="form-group">
                <label for="cidade">Curso:</label>
                <strong>{{ $course->titulo }}</strong>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="cidade" class="col-sm-2 col-form-label">Turma:</label>
        <div class="col-sm-10">
            <select class="form-control" name="class_id">
                @foreach($classes as $item)
                {{-- <option {{ $item->principal ? 'selected' : 'disabled' --}}>{{ $item->titulo }}</option> --}}
                <option value="{{ $item->id }}">{{ $item->titulo }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <button type="submit" id="nextBtn" class="btn btn-primary">Inscrever-se</button>

    <div class="feedback text-center"></div>
</form>

<style type="text/css">
    .btn-primary {
        border-color: var(--secondary);
        background-color: var(--secondary);
    }

    .btn-primary:hover {
        background-color: #d75321;
        border-color: #d75321;
    }

    form {
        margin-bottom: 50px;
    }

    form .form-check-inline label {
        margin-top: 5px;
    }
</style>
