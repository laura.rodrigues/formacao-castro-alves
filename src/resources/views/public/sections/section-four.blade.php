<section class="sectionThree mt-3" id="inscricao">
    <div class="titleContainer">
        <h5>Formulário de inscrição</h5>
    </div>

    <div class="card">
        <div class="card-body">
            @include('public.sections.form')
        </div>
    </div>
</section>

<style>
    @media (max-width: 700px) {
        .doacao {
            margin-bottom: 40px;
        }
    }
    
    .doacao {
        margin-bottom: 40px;
    }
    
</style>

<div class="text-center doacao">
    <a href="{{ url('/doacao') }}" style="padding: 10px;">
        <img src="{{ url('images/doacao-castro-alves.jpeg') }}" title="doacao castro alves" alt="doacao castro alves" width="300" />
    </a>
    {{-- <a href="{{ url('/portoalegre') }}">
        <img src="{{ url('images/seminario-covid-19.jpeg') }}" title="seminario covid-19" alt="seminario covid-19" width="300" />
    </a> --}}
</div>

<style>
    .sectionThree .titleContainer {
        text-align: center;
        background-color: #45AFB9;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100px;
        color: #fff;
        height: 45px;
    }

    .sectionThree h5 {
        margin: 5px;
        font-size: 22px;
    }
</style>
