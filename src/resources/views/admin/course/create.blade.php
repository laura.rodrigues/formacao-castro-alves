@extends('admin.layout.index')

@push('styles')
<style type="text/css">
    .card-header {
        font-weight: bold;
        text-transform: uppercase;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
        Cadastro de Curso
    </div>
    <div class="card-body">
        <form action="{{ url('administracao/curso') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="titulo">Título *</label>
                <input type="text" class="form-control" id="titulo" name="titulo" required>
            </div>

            <div class="form-group">
                <label for="introducao">Introdução</label>
                <textarea id="introducao" name="introducao"></textarea>
            </div>

            <div class="form-group">
                <label for="ementa">Ementa</label>
                <textarea id="ementa" name="ementa"></textarea>
            </div>

            <hr>

            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="status" name="status" value="1" checked>
                <label class="form-check-label" for="status">Ativo</label>
            </div>

            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="principal" name="principal" value="1">
                <label class="form-check-label" for="principal">Principal</label>
            </div>
            
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="seminario" name="seminario" value="1">
                <label class="form-check-label" for="seminario">Seminário</label>
            </div>

            <hr>

            <a href="{{ url('administracao/curso/') }}" class="btn btn-default">Voltar</a>

            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        const summernoteConfig = {
            height: 150
        };

        $('#introducao').summernote(summernoteConfig);
        $('#ementa').summernote(summernoteConfig);
    });
</script>
@endpush
