@extends('admin.layout.index')

@push('styles')
<style type="text/css">
    .card-header {
        font-weight: bold;
        text-transform: uppercase;

        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .table-md {
        margin-bottom: 0;
    }

    .table-md tbody td {
        vertical-align: middle;
    }

    .tdActions {
        width: 450px;
        text-align: right;
    }

    .modal-lg {
        max-width: 1000px;
    }

    .loading-table {
        height: 200px;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .hidden {
        display: none;
    }

    form label {
        font-weight: bold;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
        Lista de Curso

        <a href="{{ url('administracao/curso/cadastrar') }}" class="btn btn-warning">Novo</a>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-striped table-md">
                <thead class="thead-light">
                    <tr>
                        <th>Título</th>
                        <th>Qtde. Inscrições</th>
                        <th>Ativo ?</th>
                        <th>Principal ?</th>
                        <th>Data de cadastro</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $item)
                    <tr>
                        <td>{{ $item->titulo }}</td>
                        <td>{{ $item->inscriptions()->count() }}</td>
                        <td>{{ $item->status_display }}</td>
                        <td>{{ $item->principal_display }}</td>
                        <td>{{ $item->created_at_display }}</td>
                        <td class="tdActions">
                            <a href="{{ url('administracao/curso/' . $item->id . '/editar/') }}" class="btn btn-warning">Editar</a>
                            <button type="button" class="btn btn-primary btn-show-inscriptions" data-id="{{ $item->id }}" {{ $item->isNotHaveInscriptions() ? 'disabled' : ''  }}>
                                Inscrições
                            </button>
                            <button type="button" class="btn btn-dark btn-send-email" data-id="{{ $item->id }}" data-classes="{{ $item->classes }}" {{ $item->isNotHaveInscriptions() ? 'disabled' : ''  }}>
                                Enviar mensagem em lote
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInscriptionsInCourse" tabindex="-1" role="dialog" aria-labelledby="modalInscriptionsInCourseLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalInscriptionsInCourseLabel">Inscrições</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <div class="col-12 pt-3 listOfClasses">
                </div>
                <div class="form-row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Cidade</th>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Whatsapp</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="loading-table">Carregando informações ...</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSendEmailToInscriptionsInCourse" tabindex="-1" role="dialog" aria-labelledby="modalSendEmailToInscriptionsInCourseLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSendEmailToInscriptionsInCourseLabel">Enviar mensagem em lote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="formSendEmailsToInscriptions">
                    @csrf

                    <input type="hidden" name="course_id" id="course_id" value="">

                    <div class="form-group">
                        <label for="content_email">Conteúdo personalizado do email</label>
                        <p class="text-danger">
                            <small>Esta ação pode demorar dependendo da quantidade de inscrições.</small>
                        </p>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="turma">Turma</label>
                        <select class="form-control" id="turma" name="turma_id">
                            <option value="">-- SELECIONE --</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <textarea id="content_email" name="content_email"></textarea>
                    </div>

                    <div class="feedback"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary btn-submit-send-emails">Enviar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        const summernoteConfig = {
            height: 300
        };

        $('#content_email').summernote(summernoteConfig);
    });

    $('.btn-show-inscriptions').on('click', function() {
        const courseId = $(this).data('id');
        const url = window.location.origin + '/administracao/curso/' + courseId + '/inscricoes';

        $.ajax({
            type: 'GET',
            url: url,
            dataType: "json",
            cache: false,
            beforeSend: function() {
                $('#modalInscriptionsInCourse .listOfClasses').html('');
                $('#modalInscriptionsInCourse tbody').html('');
                $('.loading-table').removeClass('hidden');
            },
            success: function(response) {
                let template = '';

                let templateClasses = '';

                response.course.classes.forEach(item => {
                    templateClasses += `
                        <p class="pa-2">${item.titulo}: ${item.count_inscriptions}</p>
                    `;
                });

                response.course.inscriptions.forEach(inscription => {
                    template += `
                        <tr>
                            <td>${inscription.estado}</td>
                            <td>${inscription.cidade}</td>
                            <td>${inscription.nome}</td>
                            <td>${inscription.email}</td>
                            <td>${inscription.whatsapp}</td>
                        </tr>
                    `;
                });

                $('#modalInscriptionsInCourse .listOfClasses').html(templateClasses);

                $('#modalInscriptionsInCourse tbody').html(template);

                $('.loading-table').addClass('hidden');
            },
            error: function(response) {
                $('.loading-table').addClass('hidden');
            }
        })

        $('#modalInscriptionsInCourse').modal({
            show: true
        });
    });

    $('.btn-send-email').on('click', function() {
        let template = '';
        const classes = $(this).data('classes');
        const course_id = $(this).data('id');

        classes.filter(item => item.status == 1).forEach(item => {
            const principal = item.principal_display.toUpperCase() === 'SIM';

            template += `
                <option value="${item.id}" ${principal ? 'selected' : ''}>${item.titulo} (${item.count_inscriptions}) </option>
            `;
        });

        $('#turma').html(template);

        $('#course_id').val(course_id);

        $('#modalSendEmailToInscriptionsInCourse').modal({
            show: true
        });
    });

    $('.btn-submit-send-emails').on('click', function(event) {
        event.preventDefault();

        const $button = $(this);

        const course_id = $('#course_id').val();

        const url = window.location.origin + '/administracao/curso/' + course_id + '/enviar-emails';

        const data = $('#formSendEmailsToInscriptions').serialize();

        $.ajax({
            url,
            data,
            type: 'POST',
            dataType: "json",
            cache: false,
            beforeSend: function() {
                $button.attr('disabled', true).html('Processando');

                $('.feedback').html(`
                    <div class="alert alert-dismissible alert-info text-center">
                        <a class="close" data-dismiss="alert" aria-label="close" >×</a>
                        <span>Processando requisição... Essa ação poderá demorar algunas minutos.</span>
                    </div>
                `);
            },
            success: function(response) {
                $('.feedback').html(`
                    <div class="alert alert-dismissible alert-success text-center">
                        <a class="close" data-dismiss="alert" aria-label="close" >×</a>
                        <span>${response.message}</span>
                    </div>
                `);
            },
            error: function(error) {
                $('.feedback').html(`
                    <div class="alert alert-dismissible alert-danger text-center">
                        <a class="close" data-dismiss="alert" aria-label="close" >×</a>
                        <span>${error.response.message}</span>
                    </div>
                `);
            },
            complete: function() {
                $button.attr('disabled', false).html('Enviar');
            }
        });
    });
</script>
@endpush
