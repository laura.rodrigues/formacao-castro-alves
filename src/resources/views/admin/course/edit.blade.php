@extends('admin.layout.index')

@push('styles')
<style type="text/css">
    .card-header {
        font-weight: bold;
        text-transform: uppercase;
    }

    .table td {
        vertical-align: middle
    }

    h5 {
        font-weight: bold;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
        Edição de Curso
    </div>
    <div class="card-body">
        <form action="{{ url('administracao/curso/' . $course->id . '/') }}" method="POST">
            @csrf
            @method('PUT')

            <input type="hidden" name="turma[id]" id="turma_id" value="">

            <div class="form-row">
                <div class="col-7">
                    <div class="form-group">
                        <h5 class="text-uppercase">Curso</h5>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="titulo">Título *</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $course->titulo }}" required>
                    </div>

                    <div class="form-group">
                        <label for="introducao">Introdução</label>
                        <textarea id="introducao" name="introducao">{{ $course->introducao }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="ementa">Ementa</label>
                        <textarea id="ementa" name="ementa">{{ $course->ementa }}</textarea>
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group">
                        <h5 class="text-uppercase">Turmas</h5>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="turma_titulo">Título (*)</label>
                        <input type="text" class="form-control" id="turma_titulo" name="turma[titulo]">
                    </div>

                    <div class="form-group">
                        <label for="turma_descricao">Descrição</label>
                        <input type="text" class="form-control" id="turma_descricao" name="turma[descricao]">
                    </div>

                    <hr>

                    <div class="form-group form-check-inline d-flex">
                        <div class="col-6">
                            <input type="checkbox" class="form-check-input" id="turma_status" name="turma[status]" value="1">
                            <label class="form-check-label" for="turma_status">Ativo</label>
                        </div>
                        <div class="col-6">
                            <input type="checkbox" class="form-check-input" id="turma_principal" name="turma[principal]" value="1">
                            <label class="form-check-label" for="turma_principal">Principal</label>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Descrição</th>
                                <th>Ativo</th>
                                <th>Principal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($course->classes as $item)
                            <tr>
                                <td>{{ $item->titulo }}</td>
                                <td>{{ $item->descricao }}</td>
                                <td>{{ $item->status_display }}</td>
                                <td>{{ $item->principal_display }}</td>
                                <td><a href="#editar-turma" class="btn btn-link btn-edit-class" data-item="{{ $item }}">Editar</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <hr>

            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="status" name="status" value="1" {{ $course->status ? 'checked' : '' }}>
                <label class="form-check-label" for="status">Ativo</label>
            </div>

            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="principal" name="principal" value="1" {{ $course->principal ? 'checked' : '' }}>
                <label class="form-check-label" for="principal">Principal</label>
            </div>
            
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="seminario" name="seminario" value="1" {{ $course->seminario ? 'checked' : '' }}>
                <label class="form-check-label" for="seminario">Seminário</label>
            </div>

            <hr>

            <a href="{{ url('administracao/curso/') }}" class="btn btn-default">Voltar</a>
            @if ($course->isNotHaveInscriptions())
            <a href="{{ url('administracao/curso/' . $course->id . '/apagar/') }}" class="btn btn-danger">Apagar</a>
            @endif
            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        const summernoteConfig = {
            height: 150
        };

        $('#introducao').summernote(summernoteConfig);
        $('#ementa').summernote(summernoteConfig);
    });

    $('.btn-edit-class').on('click', function(event) {
        const item = $(this).data('item');

        $('#turma_id').val(item.id);
        $('#turma_titulo').val(item.titulo);
        $('#turma_descricao').val(item.descricao);

        if (item.status_display.toUpperCase() === 'SIM') {
            $('#turma_status').attr('checked', item.status);
        } else {
            $('#turma_status').attr('checked', false);
        }

        if (item.principal_display.toUpperCase() === 'SIM') {
            $('#turma_principal').attr('checked', item.principal);
        } else {
            $('#turma_principal').attr('checked', false);
        }
    });
</script>
@endpush
