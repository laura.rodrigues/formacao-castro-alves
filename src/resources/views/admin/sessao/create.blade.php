@extends('admin.layout.index')


@push('styles')
<style type="text/css">
    body {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .card-login {
        width: 450px;
    }

    .card-header span {
        font-weight: bold;
        text-transform: uppercase;
    }

    .btn-submit {
        height: 40px;
        margin-top: 30px;
    }
</style>
@endpush

@section('content')
<div class="card card-login">
    <div class="card-header">
        <span>Autenticação</span>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ url('administracao') }}">
            @csrf

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" class="form-control" id="senha" name="senha" required>
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-submit">Acessar</button>
        </form>
    </div>
</div>
@endsection
