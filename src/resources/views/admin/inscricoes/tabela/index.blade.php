<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade {{ $current_tab == 1 ? 'show active' : '' }}" id="assinatura-estudantil-tab" role="tabpanel" aria-labelledby="assinatura-estudantil">
        <div class="card">
            <div class="card-header">
                <a href="{{ url('administracao/curso') }}" class="btn btn-warning">Novo</a>
            </div>
            <div class="card-body">
                <div class="card-text">
                    <div class="table-responsive">
                        <table class="table table-striped table-md">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Título</th>
                                    <th scope="col">Qtde. Inscrições</th>
                                    <th scope="col">Data de cadastro</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="9">
                                    </td>
                                </tr>
                                @foreach ($cursosTab as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>0</td>
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="9">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
