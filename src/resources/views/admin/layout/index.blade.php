<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="shortcut icon" href="{{ url('/images/favicon.png') }}">
    <link rel="stylesheet" href="{{ url('/libs/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/libs/font-awesome/css/font-awesome.min.css') }}">
    <link href="{{ url('/libs/summernote/summernote.min.css') }}" rel="stylesheet">


    <title>Formação Castro Alves</title>
    <meta name='application-name' content='Formação Castro Alves'>
    <meta name='author' content='UJS'>
    <meta name='description' content='Formação Castro Alves'>
    <meta name='language' content='pt-br'>
    <meta name='twitter:site' content='Formação Castro Alves'>
    <meta name='twitter:description' content='Formação Castro Alves'>
    <meta name='twitter:domain' content='http://formacaocastroalves.com.br'>
    <meta name='apple-mobile-web-app-title' content='Formação Castro Alves'>
    <meta property='og:locale' content='pt_BR'>
    <meta property='og:title' content='Formação Castro Alves'>
    <meta property='og:description' content='Formação Castro Alves'>
    <meta property='og:url' content='http://formacaocastroalves.com.br'>
    <meta itemprop='url' content='http://formacaocastroalves.com.br'>
    <meta itemprop='image' content=''>
    <meta itemprop='description' content='Formação Castro Alves'>

    <style>
        body {
            height: 100vh;
        }

        .nav-link {
            margin-right: 5px;
        }
    </style>

    @stack('styles')
</head>

<body>

    <?php
    $isRouterCourse = substr_count(Route::current()->uri, '/curso');
    $isRouterInscription = substr_count(Route::current()->uri, '/inscricoes');
    ?>

    @if (Route::current()->uri === 'administracao')
    @yield('content')
    @else
    <div class="navs">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div class="d-none d-sm-block"></div>
                <ul class="nav nav-pills justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ $isRouterCourse ? 'active' : '' }}" href="{{ url('administracao/curso') }}">Cursos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $isRouterInscription ? 'active' : '' }}" href="{{ url('administracao/inscricoes') }}">Inscrições</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" id="formulario-publico" data-toggle="tab" href="#formulario-publico-tab" role="tab" aria-controls="formulario-publico-tab" aria-selected="true">Configuracoes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="destinatarios" data-toggle="tab" href="#destinatarios-tab" role="tab" aria-controls="destinatarios-tab" aria-selected="true">Destinatários</a>
                    </li> -->
                </ul>
                <div class="card-header-actions justify-content-center">
                    <!-- <a class="btn btn-outline-dark btn-outline btn-exportar exportar-cursos" href="{{ url('administracao/relatorios/exportar/1') }}">Exportar</a>
                    <a class="btn btn-outline-dark btn-outline btn-exportar exportar-quero-apoiar hidden" href="{{ url('administracao/relatorios/exportar/2') }}">Exportar</a> -->
                    <a class="btn btn-outline-dark btn-outline" href="{{ url('administracao/logout') }}">Logout</a>
                </div>
            </div>
            <div class="card-body p-0 m-0">
                @yield('content')
            </div>
        </div>
    </div>
    @endif


    <script src="{{ url('/libs/summernote/popper.min.js') }}"></script>
    <script src="{{ url('/libs/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ url('/libs/bootstrap.min.js') }}"></script>
    <script src="{{ url('/libs/summernote/summernote.min.js') }}"></script>

    @stack('scripts')
</body>

</html>
