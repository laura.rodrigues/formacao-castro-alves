@extends('admin.layout.index')

@push('styles')
<style>
    .container-fluid {
        padding: 0;
    }

    .card-header {
        display: flex;
        justify-content: space-between;
    }

    .card-header .nav {
        margin-left: 50px;
    }

    .card-body {
        padding: 0;
    }

    .table {
        margin-bottom: 0;
    }

    .table tr th {
        vertical-align: middle;
        text-transform: uppercase;
    }

    .table tr td {
        vertical-align: middle;
        font-size: 14px;
    }

    .table .pagination {
        justify-content: center !important;
        margin-bottom: 0 !important;
    }

    .hidden {
        display: none;
    }

    @media(max-width: 768px) {
        .card-header {
            padding: .75rem 0;
            flex-direction: column;
        }

        .card-header ul.nav {
            display: flex;
            flex-direction: column;

            padding: 0 20px;
            margin-left: 0;
        }

        .card-header ul li {
            flex: 1;
            text-align: center;
        }

        .card-header-actions {
            display: flex;
            justify-content: space-around;
            margin-top: 10px;
            padding: 0 20px;
        }

        .card-header-actions a {
            flex: 1;
            padding: 0;
        }

        .table .pagination {
            display: center;
            justify-content: flex-start !important;
        }
    }
</style>
@endpush

@section('content')
<div class="container-fluid">
    <div class="navs">
        <div class="card">
            <div class="card-header">
                <div class="d-none d-sm-block"></div>
                <ul class="nav nav-pills justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="cursos" data-toggle="tab" href="#cursos-tab" role="tab" aria-controls="cursos-tab" aria-selected="true">Cursos</a>
                    </li>
                </ul>
                <div class="card-header-actions justify-content-center">
                    <a class="btn btn-outline-dark btn-outline" href="{{ url('administracao/logout') }}">Logout</a>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="cursos-tab" role="tabpanel" aria-labelledby="cursos-tab">
                        @include('admin.inscricoes.tabela.index')
                    </div>
                    <div class="tab-pane fade" id="formulario-publico-tab" role="tabpanel" aria-labelledby="formulario-publico-tab">
                        <div class="row p-4">
                            <div class="col"></div>
                            <div class="col"></div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="destinatarios-tab" role="tabpanel" aria-labelledby="destinatarios-tab">
                        <div class="row p-4">
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $('.nav-pills .nav-link').on('click', function(ev) {
        const id = $(this).attr('id');

        $('.btn-exportar').addClass('hidden');

        $(`.exportar-${id}`).removeClass('hidden');
    });
</script>
@endpush
