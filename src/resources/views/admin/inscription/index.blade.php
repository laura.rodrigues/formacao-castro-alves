@extends('admin.layout.index')

@push('styles')
<style type="text/css">
    .card-header {
        font-weight: bold;
        text-transform: uppercase;

        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .table-md {
        margin-bottom: 0;
    }

    .table-md tbody td {
        vertical-align: middle;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-header text-left">
        Lista de Inscrições
        
        <!-- Default dropleft button -->
        <div class="btn-group dropleft">
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Exportar
            </button>
            <div class="dropdown-menu">
                @foreach($courses as $course)
                    <a class="dropdown-item" href="{{ url('/administracao/inscricoes/exportarCourses/' . $course->id) }}">{{ $course->titulo }}</a>
                @endforeach
                <a class="dropdown-item" href="{{ url('administracao/inscricoes/exportar') }}">Todos</a>
            </div>
        </div>
    </div>
    
    <div class="card-body p-0">
        <br />
        <form action="{{ url('/administracao/inscricoes') }}" method="post">
            <div class="form-group col-md-8">
                {{ csrf_field() }}
                <label class="control-label">Pesquisar</label>
                <input type="search" class="form-control input-sm" name="search" placeholder="nome ou email" value="{{ $search }}" />
            </div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary btn-sm" title="Pesquisar">Pesquisar</button>
                <a href="{{ url('/administracao/inscricoes') }}" class="btn btn-warning btn-sm" title="Limpar">Limpar</a>
            </div>        
        </form>
        
        @if($message = Session::get('sucess'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{ $message }}
                </div>
            </div>
        @endif
        
        <div class="table-responsive">
            <div class="d-flex align-items-center justify-content-center mt-3">{{ $inscriptions->appends(['search' => $search])->links() }}</div>
            <table class="table table-striped table-md">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Estado</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Whatsapp</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Turma</th>
                        <th scope="col">Data de cadastro</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inscriptions as $item)
                    <tr>
                        <td>{{ $item->estado }}</td>
                        <td>{{ $item->cidade }}</td>
                        <td>{{ $item->nome }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->whatsapp }}</td>
                        <td>{{ $item->course->titulo }}</td>
                        <td>
                            {{ $item->class->titulo }}
                        </td>
                        <td>{{ $item->created_at_display }}</td>
                        <td>
                            <form class="delete_form" method="post" action="{{ action('InscriptionController@destroy', $item->id) }}">
                                {{ csrf_field() }}
                                
                                <button type= "submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
            {{-- <div class="d-flex align-items-center justify-content-center mt-3">{{ $inscriptions->links() }}</div> --}}
            <div class="d-flex align-items-center justify-content-center mt-3">{{ $inscriptions->appends(['search' => $search])->links() }}</div>
            
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete_form').on('submit', function() {
            if(confirm("Tem certeza de que deseja remover esta inscrição?"))
            {
                return true;
            } else {
                return false;
            }
        });
    });
</script>
@endpush
