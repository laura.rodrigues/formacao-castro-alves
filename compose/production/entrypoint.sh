#!/bin/bash

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

DB_HOST=$(read_var DB_HOST .env)
DB_PORT=$(read_var DB_PORT .env)
DB_DATABASE=$(read_var DB_DATABASE .env)
DB_USERNAME=$(read_var DB_USERNAME .env)
DB_PASSWORD=$(read_var DB_PASSWORD .env)

php -r '
$dbhost = @$_ENV["DB_HOST"] ?: "mariadb";
$dbport = @$_ENV["DB_PORT"] ?: 3306;
$dbname = @$_ENV["DB_USERNAME"] ?: "bsocial";
$dbuser = @$_ENV["DB_USERNAME"] ?: "bsocial";
$dbpass = @$_ENV["DB_PASSWORD"] ?: "bsocial";

echo "\naguardando o banco de dados subir corretamente...";

while(true){
    echo "\ntestando accesso ao banco mysql:host={$dbhost};port={$dbport};dbname={$dbname};user={$dbuser};\n";
    try {
        new PDO("mysql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);
        echo "\nconectado com sucesso ao banco mysql:host={$dbhost};port={$dbport};dbname={$dbname};user={$dbuser};\n";
        break;
    } catch (Exception $e) {
        echo "..";        
    }
    sleep(1);
}
'
# sudo chown -R :www-data /var/www/html

# sudo -u www-data php artisan key:generate --force
# sudo -u www-data php artisan config:clear 
# sudo -u www-data php artisan config:cache 
# sudo -u www-data php artisan make:auth --force
# sudo -u www-data php artisan jwt:secret --force

# sudo chown -R www-data:www-data /var/www/html

exec "$@"