<?php

namespace PagarMe\Sdk\Transaction\Request;

use PagarMe\Sdk\Transaction\CreditCardTransaction;

class CreditCardTransactionCreate extends TransactionCreate
{
    /**
     * @param CreditCardTransaction $transaction
     */
    public function __construct(CreditCardTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        $basicData = parent::getPayload();
        $billing = $this->transaction->getBilling();
        $basicData['billing'] = $billing;
        $basicData['items'] = $this->transaction->getItems();

        $customer = $this->transaction->getCustomer();
        $basicData['customer'] = [
            'external_id' => "#{$customer->getExternalId()}",
            'type' => $customer->getType(),
            'country' => 'br',
            'name' => $customer->getName(),
            'email' => $customer->getEmail(),
            'documents' => [
                [
                    'type' => $customer->getDocumentType(),
                    'number' => $customer->getDocumentNumber()
                ]
            ],
            'phone_numbers' => [
                "{$customer->getPhone()}"
            ],
        ];

        $cardData = [
            'installments'    => $this->transaction->getInstallments(),
            'capture'         => $this->transaction->isCapturable(),
            'soft_descriptor' => $this->transaction->getSoftDescriptor(),
            'async' => $this->transaction->getAsync()
        ];

        if (!is_null($this->transaction->getCardCvv())) {
            $cardData['card_cvv'] = $this->transaction->getCardCvv();
        }

        return array_merge($basicData, $cardData, $this->getCardInfo());
    }

    /**
     * @return array
     */
    private function getCardInfo()
    {
        if (!is_null($this->transaction->getCardId())) {
            return ['card_id' => $this->transaction->getCardId()];
        }

        if (!is_null($this->transaction->getCardHash())) {
            return ['card_hash' => $this->transaction->getCardHash()];
        }
    }
}
