#!/bin/bash

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

DB_HOST=$(read_var DB_HOST .env)
DB_PORT=$(read_var DB_PORT .env)
DB_DATABASE=$(read_var DB_DATABASE .env)
DB_USERNAME=$(read_var DB_USERNAME .env)
DB_PASSWORD=$(read_var DB_PASSWORD .env)

php -r '
$dbhost = @$_ENV["DB_HOST"] ?: "mariadb";
$dbport = @$_ENV["DB_PORT"] ?: 3306;
$dbname = @$_ENV["DB_USERNAME"] ?: "castroalves";
$dbuser = @$_ENV["DB_USERNAME"] ?: "castroalves";
$dbpass = @$_ENV["DB_PASSWORD"] ?: "castroalves";

echo "\naguardando o banco de dados subir corretamente...";

while(true){
    echo "\ntestando accesso ao banco mysql:host={$dbhost};port={$dbport};dbname={$dbname};user={$dbuser};\n";
    try {
        new PDO("mysql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);
        echo "\nconectado com sucesso ao banco mysql:host={$dbhost};port={$dbport};dbname={$dbname};user={$dbuser};\n";
        break;
    } catch (Exception $e) {
        echo "..";        
    }
    sleep(1);
}
'

composer install
composer run post-autoload-dump

chown -R :www-data /var/www
chmod -R 775 /var/www/html/storage/framework

exec "$@"